# Using the chart

```
helm repo add kylesferrazza https://charts.kylesferrazza.com
helm show values kylesferrazza/node-red > values.yaml
# edit values.yaml
helm install kylesferrazza/node-red -f values.yaml
```
